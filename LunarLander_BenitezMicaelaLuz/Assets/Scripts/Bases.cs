﻿using System;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;

namespace BasesScript
{
    public class Bases : MonoBehaviour
    {
        public Canvas multiplierCanvas;
        private TMP_Text text;

        [NonSerialized] public int multiplier;
        private const int minMultiplier = 1;
        private const int maxMultiplier = 6;

        private Renderer render;

        private void Start()
        {
            text = multiplierCanvas.GetComponentInChildren<TMP_Text>();

            render = GetComponent<Renderer>();

            if (tag != "Base")
            {
                render.gameObject.SetActive(false);
                multiplier = 1;
            }
            else
            {
                Instantiate(multiplierCanvas, new Vector3(transform.position.x, transform.position.y - multiplierCanvas.transform.lossyScale.y * 2, transform.position.z), Quaternion.identity);

                multiplier = Random.Range(minMultiplier, maxMultiplier);
                text.text = multiplier + "X";
            }
        }
    }
}