﻿using UnityEngine;
using SpaceshipScript;

namespace CameraMovementScript
{
    public class CameraMovement : MonoBehaviour
    {
        private Spaceship spaceship;
        private int offSet = 30;
        private int distanceToBase = 10;
        private Vector3 originalPos;

        private void Awake()
        {
            spaceship = FindObjectOfType<Spaceship>();
        }

        private void Start()
        {
            originalPos = transform.position;
        }

        private void Update()
        {
            if (spaceship.altitude < distanceToBase)
                transform.position = new Vector3(spaceship.transform.position.x, spaceship.transform.position.y, spaceship.transform.position.z - offSet);
            else
                transform.position = originalPos;
        }
    }
}