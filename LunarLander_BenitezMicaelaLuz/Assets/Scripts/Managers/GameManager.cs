﻿using UnityEngine;
using MonoBehaviourSingletonScript;

namespace GameManagerScript
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        private float time;
        private bool winner;
        private int score;

        [Header("Fuel data")]
        public float fuel = 2000;
        public float fuelLoser = 100;
        private float actualFuel;

        [Header("Points data")]
        public int pointsWinner = 200;
        public int pointsLoser = 25;

        private void Start()
        {
            actualFuel = fuel;
        }

        private void Update()
        {
            time += Time.deltaTime;

            if (actualFuel < 0)
                actualFuel = 0;
        }

        public float GetTime()
        {
            return time;
        }

        public bool GetResult()
        {
            return winner;
        }

        public void SetResult(bool result)
        {
            winner = result;
        }

        public int GetScore()
        {
            return score;
        }

        public void SetScore(int quantity)
        {
            score += quantity;
        }

        public float GetFuel()
        {
            return actualFuel;
        }

        public void SetFuel(float quantity)
        {
            actualFuel += quantity;
        }
    }
}