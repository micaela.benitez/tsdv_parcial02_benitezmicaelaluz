﻿using UnityEngine;

namespace ParallaxScript
{
    public class Parallax : MonoBehaviour
    {
        public GameObject lastSprite;

        private Vector3 initialPos;
        private float finalPosX;

        public float speed = 1;

        private void Start()
        {
            initialPos = transform.localPosition;
            finalPosX = -lastSprite.transform.localPosition.x;
        }

        private void Update()
        {
            transform.position += Vector3.right * Time.deltaTime * speed;

            if (transform.localPosition.x > finalPosX)
                transform.localPosition = initialPos;
        }
    }
}