﻿using System;
using System.Collections.Generic;
using UnityEngine;
using LoaderManagerScript;
using UILoadingScreenScript;
using GameManagerScript;
using TerrainTopologyScript;
using BasesScript;

namespace SpaceshipScript
{
    public class Spaceship : MonoBehaviour, ILanding
    {
        [Header("Spaceship data")]
        public float rotationSpeed = 0.1f;
        public float gravity = 1;
        public ParticleSystem explosion;
        private float explosionTime;

        [Header("Spaceship thrusters data")]
        public float verForce = 6;
        public float horForce = 0.1f;
        public ParticleSystem particle;

        [NonSerialized] public Rigidbody2D rig;

        [NonSerialized] public bool pause;
        [NonSerialized] public bool gameOver;
        private bool loadingLandingScreen;

        private int horSpeed;
        private int verSpeed;
        private int maxHorSpeedToWin = 5;
        private int maxVerSpeedToWin = 10;

        private TerrainTopology bases;
        [NonSerialized] public float altitude;

        private int multiplier = 1;

        private void Awake()
        {
            rig = GetComponent<Rigidbody2D>();

            bases = FindObjectOfType<TerrainTopology>();
        }

        private void Start()
        {
            pause = false;
            gameOver = false;
            loadingLandingScreen = false;
            explosionTime = 0;
            multiplier = 1;
        }

        private void Update()
        {
            // Gravedad
            rig.gravityScale = gravity;

            // Input
            float ver;
            float hor;

            if (GameManager.Get().GetFuel() > 0 && !gameOver)
            {
                ver = Input.GetAxis("Vertical");
                hor = Input.GetAxis("Horizontal");
            }
            else
            {
                ver = 0;
                hor = 0;
            }

            // Movimiento vertical
            if (ver > Mathf.Epsilon || Input.GetKey(KeyCode.Space))
                Move(transform.up, verForce);

            // Movimiento horizontal
            if (hor > Mathf.Epsilon)
            {
                Move(transform.right, horForce);
            }
            else if (hor < -Mathf.Epsilon)
            {
                Move(-transform.right, horForce);
            }
            else   // Vuelve a la rotacion normal
            {
                if (rig.velocity.x > 0)
                    rig.AddForce(-transform.right * horForce);
                else if (rig.velocity.x < 0)
                    rig.AddForce(transform.right * horForce);
            }

            if (ver == 0 && hor == 0)
                particle.gameObject.SetActive(false);

            transform.rotation = Quaternion.Euler(0, 0, rig.velocity.x * 10);

            // Altitude
            CalculatedAltitude();

            // Game Over
            if (gameOver)
                GameOver();
        }

        private void Move(Vector3 direction, float force)
        {
            if (!pause)
            {
                rig.AddForce(direction * force);
                GameManager.Get().SetFuel(-0.1f);
                particle.gameObject.SetActive(true);
            }
        }

        private void CalculatedAltitude()
        {
            List<float> altitudes = new List<float>();
            float distance = 0;

            for (int i = 0; i < bases.bases.Length; i++)
                altitudes.Add(0);

            for (int i = 0; i < bases.bases.Length; i++)
            {
                Vector3 a = new Vector3(transform.position.x, transform.position.y - 3, transform.position.z);
                Vector3 b = bases.heightGO[bases.bases[i]].transform.GetChild(0).position;                
                
                distance = Vector3.Distance(a, b);
                altitudes[i] = distance;
            }

            altitudes.Sort();
            altitude = altitudes[0];

            if (altitude < 0)
                altitude = 0;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (horSpeed > -maxHorSpeedToWin && horSpeed < maxHorSpeedToWin && verSpeed > -maxVerSpeedToWin && verSpeed < maxVerSpeedToWin && collision.gameObject.tag == "Base")
            {
                GameManager.Get().SetResult(true);
                PointsEarned(GameManager.Get().pointsWinner * multiplier);
            }
            else
            {
                GameManager.Get().SetResult(false);
                PointsEarned(GameManager.Get().pointsLoser / multiplier);
                GameManager.Get().SetFuel(-GameManager.Get().fuelLoser * multiplier);
                Instantiate(explosion, transform.position, Quaternion.identity);
            }

            gameOver = true;
        }

        private void GameOver()
        {
            if (!GameManager.Get().GetResult() && !loadingLandingScreen)
                explosionTime += Time.deltaTime;

            if (explosionTime > explosion.main.duration || GameManager.Get().GetResult())
            {
                explosionTime = 0;
                loadingLandingScreen = true;
                UILoadingScreen.Get().SetVisible(true);
                LoaderManager.Get().LoadScene("Landing");

                gameOver = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)   // Esto es para saber si gane o perdi, porque si lo chequeo todo en la colision, las velocidades siempre dan 0
        {
            if (collision.tag != "Wind")
            {
                horSpeed = (int)rig.velocity.x;
                verSpeed = (int)rig.velocity.y;

                if (collision.tag == "Base")
                {
                    Bases baseCollision = collision.GetComponent<Bases>();
                    multiplier = baseCollision.multiplier;
                }
            }
        }

        public void PointsEarned(int points)
        {
            GameManager.Get().SetScore(points);
        }
    }
}