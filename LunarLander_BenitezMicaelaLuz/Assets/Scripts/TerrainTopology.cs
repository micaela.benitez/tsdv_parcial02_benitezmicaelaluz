﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TerrainTopologyScript
{
    public class TerrainTopology : MonoBehaviour
    {
        private const int length = 100;   // Cantidad de rectangulos
        private const int neighboursRadius = 3;   // Este valor cambia el impacto del suavizado

        // Tamaño cubos
        private const int minHeightRec = 1;
        private const int maxHeightRec = 70;
        private const int width = 4;

        private int[] heightmap = new int[length / width];
        [NonSerialized] public GameObject[] heightGO = new GameObject[length / width];

        public GameObject cubePrefab;
        public Material baseMaterial;

        // Bases
        private const int totalBases = 4;
        [NonSerialized] public int[] bases = new int[totalBases];

        private void Start()
        {
            for (int i = 0; i < heightmap.Length; i++)
            {
                heightmap[i] = Random.Range(minHeightRec, maxHeightRec);

                // Creo los rectangulos
                GameObject go = Instantiate(cubePrefab, transform.position, Quaternion.identity);   // Creo un game object con un cubo
                go.transform.parent = gameObject.transform;

                heightGO[i] = go;   // Agrego el cubo al array
                TransformGO(i);   // Coloca y dimensiona cada cubo
            }

            Smooth();

            CalculateBases();
            AssignBases();
        }

        private void CalculateBases()
        {
            for (int i = 0; i < bases.Length; i++)
                bases[i] = Random.Range(0, heightGO.Length);
        }

        private void AssignBases()
        { 
            for (int i = 0; i < heightGO.Length; i++)
            {
                for (int j = 0; j < bases.Length; j++)
                {
                    if (i == bases[j])
                    {
                        heightGO[i].tag = "Base";
                        heightGO[i].transform.GetChild(0).tag = "Base";
                    }
                }
            }
        }

        private void TransformGO(int i)
        {
            GameObject gameObject = heightGO[i];   // Le asigna el cubo a un game object
            gameObject.transform.localScale = new Vector2(width, heightmap[i]);   // Ajusta el tamaño
            gameObject.transform.localPosition = new Vector2((i * width) + width / 2.0f, heightmap[i] / 2.0f);   // Ajusta la posicion
            gameObject.name = "Cube " + i + " (height=" + heightmap[i] + ")";
        }

        void Smooth()   // Cambia la altura de los cubos
        {
            for (int i = 0; i < heightmap.Length; i++)
            {
                int heightSum = 0;   // Suma de la altura de los cubos vecinos
                int heightCount = 0;   // Cantidad de cubos vecinos ()

                for (int j = i - neighboursRadius; j < i + neighboursRadius + 1; j++)   // Chequeo los cubos vecinos dentro del radio que se quiera
                {
                    if (j >= 0 && j < heightmap.Length)   // Chequeo que el for no se vaya del rango
                    {
                        int neighbourHeight = heightmap[j];   // Altura del cubo vecino

                        heightSum += neighbourHeight;
                        heightCount++;
                    }
                }

                int heightAverage = heightSum / heightCount;   // Promedio de la suma de la altura de los cubos vecinos + cantidad de cubos vecinos
                heightmap[i] = heightAverage;   // Reemplazamos la altura antigua por la nueva altura obetenida

                TransformGO(i);   // Actualizo la altura del cubo
            }
        }
    }
}