﻿using UnityEngine;
using LoaderManagerScript;
using UILoadingScreenScript;

namespace UICreditsScript
{
    public class UICredits : MonoBehaviour
    {
        public void LoadMainMenuScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}