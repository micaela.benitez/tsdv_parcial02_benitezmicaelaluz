﻿using UnityEngine;
using GameManagerScript;
using TMPro;
using SpaceshipScript;

namespace UIGameScript
{
    public class UIGame : MonoBehaviour
    {
        public Canvas pauseScreen;
        public Canvas refillFuel;

        public TMP_Text score;
        public TMP_Text time;
        public TMP_Text fuel;
        public TMP_Text altitude;
        public TMP_Text horizontalSpeed;
        public TMP_Text verticalSpeed;

        private Spaceship spaceship;

        private void Awake()
        {
            spaceship = FindObjectOfType<Spaceship>();
        }

        private void Start()
        {
            if (GameManager.Get().GetFuel() == 0)
            {
                Time.timeScale = 0;
                refillFuel.gameObject.SetActive(true);
            }
        }

        private void Update()
        {
            score.text = "Score " + GameManager.Get().GetScore();
            time.text = "Time " + GameManager.Get().GetTime().ToString("F");
            fuel.text = "Fuel " + (int)GameManager.Get().GetFuel();
            altitude.text = "Altitude " + (int)spaceship.altitude;
            horizontalSpeed.text = "Horizontal speed " + "\n" + (int)spaceship.rig.velocity.x;
            verticalSpeed.text = "Vertical speed " + "\n" + (int)spaceship.rig.velocity.y;
        }

        public void LoadPauseScreen()
        {
            if (!spaceship.gameOver)
            {
                Time.timeScale = 0;
                pauseScreen.gameObject.SetActive(true);
                spaceship.pause = true;
            }
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}