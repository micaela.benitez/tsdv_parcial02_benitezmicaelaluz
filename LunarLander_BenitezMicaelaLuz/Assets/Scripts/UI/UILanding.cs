﻿using UnityEngine;
using LoaderManagerScript;
using UILoadingScreenScript;
using GameManagerScript;
using TMPro;

namespace UILandingScript
{
    public class UILanding : MonoBehaviour
    {
        [Header("Winner canvas data")]
        public Canvas winner;
        public TMP_Text winnerPoints;

        [Header("Loser canvas data")]
        public Canvas loser;
        public TMP_Text fuelLost;
        public TMP_Text loserPoints;

        private void Start()
        {
            if (GameManager.Get().GetResult())
            {
                winner.gameObject.SetActive(true);
                loser.gameObject.SetActive(false);
                winnerPoints.text = GameManager.Get().pointsWinner + " Points";
            }
            else
            {
                loser.gameObject.SetActive(true);
                winner.gameObject.SetActive(false);
                fuelLost.text = GameManager.Get().fuelLoser + " Fuel units lost";
                loserPoints.text = GameManager.Get().pointsLoser + " Points";
            }
        }

        public void LoadMainMenuScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void LoadNextLevelScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("Game");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}