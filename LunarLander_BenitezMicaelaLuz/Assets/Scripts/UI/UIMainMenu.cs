﻿using UnityEngine;
using LoaderManagerScript;
using UILoadingScreenScript;

namespace UIMainMenuScript
{
    public class UIMainMenu : MonoBehaviour
    {
        public void LoadGameScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("Game");

        }

        public void LoadCreditsScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("Credits");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}