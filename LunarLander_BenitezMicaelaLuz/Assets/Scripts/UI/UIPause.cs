﻿using UnityEngine;
using LoaderManagerScript;
using UILoadingScreenScript;
using SpaceshipScript;

namespace UIPauseScript
{
    public class UIPause : MonoBehaviour
    {
        private Spaceship spaceship;

        private void Awake()
        {
            spaceship = FindObjectOfType<Spaceship>();
        }

        public void LoadMainMenuScene()
        {
            DesactivatedPauseScreen();
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void ContinueGame()
        {
            DesactivatedPauseScreen();
            gameObject.SetActive(false);
        }

        public void DesactivatedPauseScreen()
        {
            Time.timeScale = 1;
            spaceship.pause = false;
        }
    }
}