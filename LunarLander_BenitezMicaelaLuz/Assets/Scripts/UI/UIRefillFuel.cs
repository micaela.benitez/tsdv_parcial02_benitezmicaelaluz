﻿using UnityEngine;
using TMPro;
using GameManagerScript;

namespace UIRefillFuelScript
{
    public class UIRefillFuel : MonoBehaviour
    {
        public TMP_Text fuel;

        private void Start()
        {
            fuel.text = GameManager.Get().fuel + " fuel units";
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Time.timeScale = 1;
                GameManager.Get().SetFuel(GameManager.Get().fuel);
                gameObject.SetActive(false);
            }
        }
    }
}