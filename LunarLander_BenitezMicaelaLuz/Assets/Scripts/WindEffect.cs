﻿using UnityEngine;

namespace WindEffectScript
{
    public class WindEffect : MonoBehaviour
    {
        public Effector2D windEffect;

        private bool windAreasActivated;

        private enum Rotation { LEFT, RIGHT }
        private Rotation rot;

        private int quantity;
        private const int minQuantity = 1;
        private const int maxQuantity = 2;
                
        private const int minPosX = 5;
        private const int maxPosX = 95;
        private const int minPosY = 65;
        private const int maxPosY = 95;

        private const int minMagnitude = 6;
        private const int maxMagnitude = 10;

        private void Start()
        {
            windAreasActivated = Random.Range(0, 2) == 0;

            if (!windAreasActivated)   // No hay zonas de viento en el nivel
            {
                gameObject.SetActive(false);
            }
            else   // Hay zonas de viento en el nivel
            {
                quantity = Random.Range(minQuantity, maxQuantity + 1);

                for (int i = 0; i < quantity; i++)
                {
                    rot = (Rotation)Random.Range(0, 2);

                    if (rot == Rotation.LEFT)
                        CreatedWindAreaEffect(Quaternion.Euler(0, 0, 180));
                    else
                        CreatedWindAreaEffect(Quaternion.identity);
                }
            }
        }

        private void CreatedWindAreaEffect(Quaternion quaternion)
        {
            Effector2D effect = Instantiate(windEffect, new Vector2(Random.Range(minPosX, maxPosX), Random.Range(minPosY, maxPosY)), quaternion);
            effect.GetComponent<AreaEffector2D>().forceMagnitude = Random.Range(minMagnitude, maxMagnitude + 1);
        }
    }
}